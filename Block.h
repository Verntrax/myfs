//
// Created by pkarwack on 05.01.18.
//

#ifndef MYFS_BLOCK_H
#define MYFS_BLOCK_H

union Block {
    char charArray[8];
    struct {
        unsigned int used;
        unsigned int nextAddress;
    };
};

#endif //MYFS_BLOCK_H
