//
// Created by pkarwack on 05.01.18.
//

#ifndef MYFS_FILE_H
#define MYFS_FILE_H

union File {
    char charArray[12];
    struct {
        char name[8];
        unsigned int address;
    };
};

#endif //MYFS_FILE_H
