//
// Created by pkarwack on 05.01.18.
//

#ifndef MYFS_FILESYSTEM_H
#define MYFS_FILESYSTEM_H

union FileSystem {
    char charArray[12];
    struct {
        unsigned int space;
        unsigned int occSpace;
        unsigned int numOfFiles;
    };
};

#endif //MYFS_FILESYSTEM_H
