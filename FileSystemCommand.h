//
// Created by pkarwack on 05.01.18.
//

#ifndef MYFS_FILESYSTEMCOMMAND_H
#define MYFS_FILESYSTEMCOMMAND_H

#include "FileSystemMaker.h"

class FileSystemCommand {
    bool fsExists(const std::string&);
    int doCreateFs(int, char**);
    int doCopyToFs(int, char**);
    int doCopyFromFs(int, char**);
    int doShowFiles(int, char**);
    int doDeleteFile(int, char**);
    int doDeleteFs(int, char**);
    int doShowStats(int, char**);

public:
    int operator()(int,char**);
};

#endif //MYFS_FILESYSTEMCOMMAND_H
