//
// Created by pkarwack on 05.01.18.
//

#include <fstream>
#include <iostream>
#include <cstring>
#include <iomanip>
#include "FileSystemMaker.h"


using namespace std;

void FileSystemMaker::writeFsMetadata() const {
    fstream outputFile(name,fstream::in | fstream::out | fstream::binary);
    outputFile.write(fileSystem.charArray,sizeof(FileSystem));
    outputFile.close();
}

void FileSystemMaker::writeFileMetadata() const {
    fstream outputFile(name,fstream::in | fstream::out | fstream::binary);
    outputFile.seekp(fileOffset);
    for (const auto &fileData : files) {
        File file{};
        strcpy(file.name,fileData.first.c_str());
        file.address = fileData.second;
        outputFile.write(file.charArray,sizeof(File));
    }
    outputFile.close();
}

void FileSystemMaker::writeBlockMetadata() const {
    fstream outputFile(name,fstream::in | fstream::out | fstream::binary);
    outputFile.seekp(blockOffset);
    for (const auto &block : blocks)
        outputFile.write(block.charArray,sizeof(Block));
    outputFile.close();
}

void FileSystemMaker::initializeFile() const {
    ofstream file(name,ofstream::binary);
    for (int i = 0; i < sizeof(FileSystem); ++i)
        file << 's';
    for (int i = 0; i < fileSystem.space*sizeof(File); ++i)
        file << 'f';
    for (int i = 0; i < fileSystem.space*sizeof(Block); ++i)
        file << 'b';
    for (int i = 0; i < fileSystem.space; ++i)
        for (int j = 0; j < blockSize; ++j)
            file << 'x';
    file.close();
}

unsigned int FileSystemMaker::findEmptyBlock(unsigned int start) const {
    for (unsigned int i = start; i < fileSystem.space; ++i)
        if (blocks[i].used == 0)
            return i;
}

FileSystemMaker::FileSystemMaker(const string &name)
        : name(name)
{
    fstream sourceFile(name,fstream::in | fstream::out | fstream::binary);
    sourceFile.read(fileSystem.charArray,sizeof(fileSystem));

    fileOffset = sizeof(FileSystem);
    blockOffset = fileOffset + sizeof(File)*fileSystem.space;
    dataOffset = blockOffset + sizeof(Block)*fileSystem.space;

    for (int i = 0; i < fileSystem.numOfFiles; ++i) {
        File file{};
        sourceFile.read(file.charArray,sizeof(File));
        files.insert(pair<string,unsigned int>(file.name,file.address));
    }
    sourceFile.seekg(blockOffset);
    for (int i = 0; i < fileSystem.space; ++i) {
        Block block{};
        sourceFile.read(block.charArray, sizeof(Block));
        blocks.push_back(block);
    }
    sourceFile.close();
}

FileSystemMaker::FileSystemMaker(const string &name, const unsigned int size)
        : name(name)
{
    fileSystem.space = size;
    fileSystem.occSpace = 0;
    fileSystem.numOfFiles = 0;

    fileOffset = sizeof(FileSystem);
    blockOffset = fileOffset + sizeof(File)*size;
    dataOffset = blockOffset + sizeof(Block)*size;

    for (unsigned int i = 0; i < size; ++i)
        blocks.emplace_back();
}

int FileSystemMaker::createFs() const {
    initializeFile();
    writeFsMetadata();
    writeFileMetadata();
    writeBlockMetadata();
    return 0;
}

int FileSystemMaker::copyToFs(const std::string &sourceName, const std::string &targetName) {
    ifstream input(sourceName,ifstream::ate | ifstream::binary);
    if (!input)
        return 1;
    if (targetName.size() > 8) {
        input.close();
        return 2;
    }
    if (files.count(targetName) != 0) {
        input.close();
        return 3;
    }
    auto requiredBytes = (unsigned int)input.tellg();
    input.seekg(input.beg);
    unsigned int requiredBlocks = requiredBytes/blockSize;

    if (requiredBytes > requiredBlocks*blockSize)
        ++requiredBlocks;
    if (requiredBlocks > fileSystem.space - fileSystem.occSpace) {
        input.close();
        return 4;
    }
    unsigned int address = findEmptyBlock(0);
    files[targetName] = address;
    ++fileSystem.numOfFiles;

    fstream output(name,fstream::in | fstream::out | fstream::binary);
    char buffer[blockSize];

    while (requiredBytes > blockSize) {
        input.read(buffer,blockSize);

        output.seekp(dataOffset + address*blockSize);
        output.write(buffer,blockSize);

        blocks[address].used = blockSize;
        unsigned int nextAddress = findEmptyBlock(address+1);
        blocks[address].nextAddress = nextAddress;
        address = nextAddress;

        requiredBytes -= blockSize;
        ++fileSystem.occSpace;
    }
    input.read(buffer,requiredBytes);
    input.close();

    output.seekp(dataOffset + address*blockSize);
    output.write(buffer,requiredBytes);
    output.close();

    blocks[address].used = requiredBytes;
    blocks[address].nextAddress = address;
    ++fileSystem.occSpace;

    writeFsMetadata();
    writeFileMetadata();
    writeBlockMetadata();

    return 0;
}

int FileSystemMaker::copyFromFs(const std::string &sourceName, const std::string &targetName) const {
    if (files.count(sourceName) == 0)
        return 1;

    fstream input(name,fstream::in | fstream::out | fstream::binary);
    ofstream output(targetName,ofstream::binary);

    char buffer[blockSize];
    unsigned int address = files.at(sourceName);

    while (address != blocks[address].nextAddress) {
        input.seekg(dataOffset + address*blockSize);
        input.read(buffer,blockSize);
        output.write(buffer,blockSize);

        address = blocks[address].nextAddress;
    }
    input.seekg(dataOffset + address*blockSize);
    input.read(buffer,blocks[address].used);
    output.write(buffer,blocks[address].used);
    input.close();
    output.close();

    return 0;
}

int FileSystemMaker::showFiles() const {
    for (const auto &file : files) {
        unsigned  int size = 0;
        unsigned int address = file.second;
        while (blocks[address].nextAddress != address) {
            address = blocks[address].nextAddress;
            size += blockSize;
        }
        size += blocks[address].used;
        cout << setw(8) << left << file.first << "\t";
        cout << setw(11) << right << size << "B" << "\t";
        cout << setw(10) << right << file.second << endl;
    }
    return 0;
}

int FileSystemMaker::deleteFile(const string &fileName) {
    if (files.count(fileName) == 0)
        return 1;

    unsigned int address = files[fileName];
    files.erase(fileName);
    --fileSystem.numOfFiles;

    while (address != blocks[address].nextAddress) {
        blocks[address].used = 0;
        address = blocks[address].nextAddress;
        --fileSystem.occSpace;
    }
    blocks[address].used = 0;
    --fileSystem.occSpace;

    writeFsMetadata();
    writeFileMetadata();
    writeBlockMetadata();

    return 0;
}

int FileSystemMaker::showStats() const {
    cout << "Data blocks in use:" << endl;
    cout << fileSystem.occSpace << "/" << fileSystem.space << endl << endl;

    cout << "Metadata blocks:" << endl;
    cout <<  "FSMD\t";
    cout << setw(11) << right << fileOffset << "B" << endl;
    cout <<  "FMD \t";
    cout << setw(11) << right << blockOffset - fileOffset << "B" << endl;
    cout << "BMD \t";
    cout << setw(11) << right << dataOffset - blockOffset << "B" << endl << endl;

    cout << "Data blocks:" << endl;
    for (unsigned int i = 0; i < fileSystem.space; ++i) {
        cout << setw(10) << left << i << "\t";
        cout << setw(9) << right << blocks[i].used << "/" << blockSize << endl;
    }
    return 0;
}
