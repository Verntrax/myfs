//
// Created by pkarwack on 05.01.18.
//

#include <iostream>
#include <fstream>
#include <cstdio>
#include "FileSystemCommand.h"

using namespace std;

bool FileSystemCommand::fsExists(const std::string &name) {
    ifstream file(name);
    if (file) {
        file.close();
        return true;
    }
    cerr << "File system does not exist" << endl;
    return false;
}

int FileSystemCommand::doCreateFs(int argc, char **argv) {
    if (argc < 4) {
        cerr << "Not enough input arguments" << endl;
        return 1;
    }
    unsigned int size;
    try {
        size = (unsigned int)stoul(argv[3]);
    } catch (...) {
        cerr << "Invalid size" << endl;
        return 1;
    }
    FileSystemMaker fs(argv[2],size);
    return fs.createFs();
}

int FileSystemCommand::doCopyToFs(int argc, char **argv) {
    if (argc < 5) {
        cerr << "Not enough input arguments" << endl;
        return 1;
    }
    if (!fsExists(argv[2]))
        return 1;

    FileSystemMaker fs(argv[2]);

    switch (fs.copyToFs(argv[3],argv[4])) {
        case 1:
            cerr << "Source file does not exist" << endl;
            break;
        case 2:
            cerr << "Target file name too long - max is 8 chars" << endl;
            break;
        case 3:
            cerr << "Target file already exists" << endl;
            break;
        case 4:
            cerr << "Not enough space in file system" << endl;
            break;
        default:
            return 0;
    }
    return 1;
}

int FileSystemCommand::doCopyFromFs(int argc, char **argv) {
    if (argc < 5) {
        cerr << "Not enough input arguments" << endl;
        return 1;
    }
    if (!fsExists(argv[2]))
        return 1;

    FileSystemMaker fs(argv[2]);

    switch (fs.copyFromFs(argv[3],argv[4])) {
        case 1:
            cerr << "Source file does not exist" << endl;
            break;
        default:
            return 0;
    }
    return 1;
}

int FileSystemCommand::doShowFiles(int argc, char **argv) {
    if (!fsExists(argv[2]))
        return 1;

    FileSystemMaker fs(argv[2]);
    return fs.showFiles();
}

int FileSystemCommand::doDeleteFile(int argc, char **argv) {
    if (argc < 4) {
        cerr << "Not enough input arguments" << endl;
        return 1;
    }
    if (!fsExists(argv[2]))
        return 1;

    FileSystemMaker fs(argv[2]);

    switch (fs.deleteFile(argv[3])) {
        case 1:
            cerr << "File does not exist" << endl;
            break;
        default:
            return 0;
    }
    return 1;
}

int FileSystemCommand::doDeleteFs(int argc, char **argv) {
    if (!fsExists(argv[2]))
        return 1;
    return remove(argv[2]);
}

int FileSystemCommand::doShowStats(int argc, char **argv) {
    if (!fsExists(argv[2]))
        return 1;

    FileSystemMaker fs(argv[2]);
    return fs.showStats();
}

int FileSystemCommand::operator()(int argc, char **argv) {
    if (argc < 3) {
        cerr << "Not enough input arguments" << endl;
        return 1;
    }
    string command = argv[1];
    if (command == "create")
        return doCreateFs(argc,argv);
    else if (command == "cptofs")
        return doCopyToFs(argc,argv);
    else if (command == "cpfromfs")
        return doCopyFromFs(argc,argv);
    else if (command == "files")
        return doShowFiles(argc,argv);
    else if (command == "delfile")
        return doDeleteFile(argc,argv);
    else if (command == "delfs")
        return doDeleteFs(argc,argv);
    else if (command == "stats")
        return doShowStats(argc,argv);
    else {
        cerr << "Command not found" << endl;
        return 1;
    }
}
