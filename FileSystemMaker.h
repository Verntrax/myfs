//
// Created by pkarwack on 05.01.18.
//

#ifndef MYFS_FILESYSTEMMAKER_H
#define MYFS_FILESYSTEMMAKER_H

#include <tuple>
#include <string>
#include <unordered_map>
#include <vector>
#include "FileSystem.h"
#include "File.h"
#include "Block.h"

class FileSystemMaker {
    static const unsigned int blockSize = 4096;

    const std::string name;

    FileSystem fileSystem;

    unsigned int fileOffset;
    unsigned int blockOffset;
    unsigned int dataOffset;

    std::unordered_map<std::string,unsigned int> files;
    std::vector<Block> blocks;

    void writeFsMetadata() const;
    void writeFileMetadata() const;
    void writeBlockMetadata() const;
    void initializeFile() const;
    unsigned int findEmptyBlock(unsigned int) const;

public:
    explicit FileSystemMaker(const std::string&);
    FileSystemMaker(const std::string&, unsigned int);

    int createFs() const;
    int copyToFs(const std::string&, const std::string&);
    int copyFromFs(const std::string&, const std::string&) const;
    int showFiles() const;
    int deleteFile(const std::string&);
    int showStats() const;
};

#endif //MYFS_FILESYSTEMMAKER_H
